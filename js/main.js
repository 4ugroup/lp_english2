$(".btn-modal").fancybox({
    'padding'    : 0,
    'tpl'        : {
        wrap     : '<div class="fancybox-wrap res-fancybox-wrap" tabIndex="-1"><div class="fancybox-skin res-fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner res-fancybox-inner"></div></div></div></div>'
    }
});

$(".btn-res").fancybox({
    'padding'    : 0,
    'tpl'        : {
        wrap     : '<div class="fancybox-wrap res-fancybox-wrap" tabIndex="-1"><div class="fancybox-skin res-fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner res-fancybox-inner"></div></div></div></div>',
        closeBtn : '<a title="Close" class="btn-close" href="javascript:;"></a>'
    }
});

//  Slider

$('.review').slick({
    arrows: false,
    autoplay: false,
    dots: false,
    vertical: true,
    slidesToShow: 1,
    slidesToScroll: 1
});

$('.btn-review').click(function(e){
    e.preventDefault();
    $('.review').slick('slickNext');
});